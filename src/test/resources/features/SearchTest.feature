Feature: Search
  Search for passed phrase and redirect to the search results page

  Scenario: Should return non empty search results when looking for existing product
    Given Open homepage
    When I search using "Dress" phrase
    Then Results page shows non empty products list

  Scenario: Should return non empty search results when looking for existing product
    Given Open homepage
    When I search using "Ferrari" phrase
    Then Results page shows empty products list

#  TODO:
#  add cucumber keyword for method searchResultsPage.getValueOfFoundProductsCounter()
#  use this keyword in above tests
#  add 1 new test using exiting keywords (search of different word) - you can use screnarion outline