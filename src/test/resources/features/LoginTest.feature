Feature: Login user
  Login user when valid credentials are provided.

  Scenario: Should Login User When Correct Credentials Are Provided
    Given Open login page
    When Login with email "test@softie.pl" and password "1qaz!QAZ"
    Then User is logged in

  Scenario Outline: Should display correct error messages
    Given Open login page
    When Login with email <email> and password <password>
    Then Error message <expectedError> is shown

    Examples:
      | email             | password    | expectedError               |
      | "test@softie.pl"  | ""          | "Password is required"      |
      | "test@softie.pl"  | "wrongPass" | "Authentication failed"     |
      | ""                | ""          | "An email address required" |
      | "test"            | "wrongPass" | "Invalid email address"     |

# TODO:
#  add test for logout