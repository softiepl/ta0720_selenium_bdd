Feature: Register user
  Should register new user

  Scenario: Positive registration
    Given Open login page
    When Register new user with mandatory data only
    Then User is logged in

#  TODO:
#  add tests:
#  1. check if correct error messages are displayed when only half of mandatory data is provided
#  2. check postcode validation (try to use a scenario outline)
#  3. test that will use new method created in RegisterSteps - check TODO section in steps\RegisterSteps.java