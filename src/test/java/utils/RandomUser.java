package utils;

import com.github.javafaker.Faker;

//TODO:
//add here all fields needed for register new user
//fields should be set in constructor for random values (faker can be used)
//update toString class to show all fields

public class RandomUser {
    public String firstName;
    public String lastName;
    public String password = "1qaz!QAZ";
    public String address1;

    public RandomUser() {
        Faker faker = new Faker();
        this.firstName = faker.name().firstName();
        this.lastName = faker.name().lastName();
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", address1='" + address1 + '\'' +
                '}';
    }
}
