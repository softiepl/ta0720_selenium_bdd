package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

public class RegisterPage extends BasePage {

    @FindBy(id = "customer_firstname")
    private WebElement firstNameInputField;

    @FindBy(id = "customer_lastname")
    private WebElement lastNameInputField;

    @FindBy(id = "passwd")
    private WebElement passwordInputField;

    @FindBy(id = "address1")
    private WebElement address1InputField;

    @FindBy(id = "id_state")
    private WebElement stateSelectField;

    @FindBy(id = "city")
    private WebElement cityInputField;

    @FindBy(id = "postcode")
    private WebElement postcodeInputField;

    @FindBy(id = "phone_mobile")
    private WebElement phoneMobileInputField;

    @FindBy(id = "submitAccount")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='alert alert-danger']")
    private WebElement errorMessage;

    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public void registerNewUser(String firstName, String lastName, String password, String address1,
                                String city, String postcode, String state, String phoneNumber) {
        wait.until(ExpectedConditions.elementToBeClickable(firstNameInputField));

        firstNameInputField.clear();
        firstNameInputField.sendKeys(firstName);
        lastNameInputField.clear();
        lastNameInputField.sendKeys(lastName);
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
        address1InputField.clear();
        address1InputField.sendKeys(address1);
        cityInputField.clear();
        cityInputField.sendKeys(city);
        postcodeInputField.clear();
        postcodeInputField.sendKeys(postcode);

        Select stateSelect = new Select(stateSelectField);
        stateSelect.selectByVisibleText(state);

        phoneMobileInputField.clear();
        phoneMobileInputField.sendKeys(phoneNumber);

        clickOnRegisterButton();
    }

    public void registerNewUser(RandomUser randomUser) {
        wait.until(ExpectedConditions.elementToBeClickable(firstNameInputField));

        firstNameInputField.clear();
        firstNameInputField.sendKeys(randomUser.firstName);
        lastNameInputField.clear();
        lastNameInputField.sendKeys(randomUser.lastName);
    }

    public boolean isRegisterErrorDisplayed() {
        return errorMessage.isDisplayed();
    }

    public boolean isMissingPasswordErrorDisplayed() {
        return errorMessage.getText().contains("passwd is required.");
    }

    public boolean isMissingCityErrorDisplayed() {
        return errorMessage.getText().contains("city is required.");
    }

    public boolean isMissingAddress1ErrorDisplayed() {
        return errorMessage.getText().contains("address1 is required.");
    }

    public boolean isInvalidPostcodeErrorDisplayed() {
        return errorMessage.getText().contains("The Zip/Postal code you've entered is invalid. It must follow this format: 00000");
    }

    public boolean isMissingStateErrorDisplayed() {
        return errorMessage.getText().contains("This country requires you to choose a State.");
    }

    public boolean isMissingPhoneNumberErrorDisplayed() {
        return errorMessage.getText().contains("You must register at least one phone number.");
    }

    private void clickOnRegisterButton() {
        submitButton.click();
    }


}

