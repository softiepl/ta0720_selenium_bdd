package steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.BasePage;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;

public class LoginSteps {

    private WebDriver driver;
    private WebDriverWait wait ;
    private LoginPage loginPage;
    private BasePage basePage;

    // PicoContainer injects BaseStep class
    public LoginSteps(BaseSteps baseSteps) {
        this.driver = baseSteps.driver;
        this.wait = baseSteps.wait;
        this.basePage = baseSteps.basePage;
        this.loginPage = baseSteps.loginPage;
    }

    @When("Login with email {string} and password {string}")
    public void login_with_email_and_password(String email, String password) {
        loginPage.login(email, password);
    }

    @Then("Error message {string} is shown")
    public void error_message_is_shown(String expectedErrorMessage) {
        Assertions.assertTrue(loginPage.isErrorMessageDisplayed(expectedErrorMessage));
    }


}
