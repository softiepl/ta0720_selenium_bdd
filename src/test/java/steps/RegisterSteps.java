package steps;

import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.BasePage;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;
import utils.RandomUser;

public class RegisterSteps {

    private WebDriver driver;
    private WebDriverWait wait;
    private BasePage basePage;
    private LoginPage loginPage;
    private RegisterPage registerPage;

    // PicoContainer injects BaseStep class
    public RegisterSteps(BaseSteps baseSteps) {
        this.driver = baseSteps.driver;
        this.wait = baseSteps.wait;
        this.basePage = baseSteps.basePage;
        this.loginPage = baseSteps.loginPage;
        this.registerPage = baseSteps.registerPage;
    }

    @Given("Register new user with mandatory data only")
    public void register_new_user_with_mandatory_data_only() {
        registerPage = loginPage.goToRegisterPage("@wp.pl");
        registerPage.registerNewUser("Jan", "Kowalski", "1qaz!QAZ", "Wojska Polskiego 12",
                "Warszawa", "02123", "Florida", "12412341");
    }

//    TODO:
//    add new method (like registerNewUserWithRandomData(RandomUser user) that will use RandomUser object utils\RandomUser.java
}
