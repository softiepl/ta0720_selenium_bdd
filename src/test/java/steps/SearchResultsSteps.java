package steps;

import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.SearchResultsPage;

public class SearchResultsSteps {

    private WebDriver driver;
    private WebDriverWait wait;
    private SearchResultsPage searchResultsPage;

    // PicoContainer injects BaseStep class
    public SearchResultsSteps(BaseSteps baseSteps) {
        this.driver = baseSteps.driver;
        this.wait = baseSteps.wait;
        this.searchResultsPage = baseSteps.searchResultsPage;
    }

    @Then("Results page shows non empty products list")
    public void results_page_shows_non_empty_products_list() {
        Assertions.assertTrue(searchResultsPage.isSearchResultsListNotEmpty());
    }

    @Then("Results page shows empty products list")
    public void results_page_shows_empty_products_list() {
        Assertions.assertTrue(searchResultsPage.isSearchResultsListEmpty());
    }

}
