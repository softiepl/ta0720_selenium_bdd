package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.*;

import java.util.concurrent.TimeUnit;

public class BaseSteps {

    protected BasePage basePage;
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected LoginPage loginPage;
    protected RegisterPage registerPage;
    protected HomePage homePage;
    protected SearchResultsPage searchResultsPage;

    @Before
    public void setup() {
        driver = new ChromeDriver();
//      TODO:
//      create method prepareWebDriver where we can pass string param and will return correct webDriver
//      method should be able to start chromeDriver for 'chrome', firefoxDriver for 'ff' and chrome headless for 'headless'
//      use this method here instead of driver = new ChromeDriver(); (so now it will be e.g. driver = prepareDriver('chrome') )
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Then("User is logged in")
    public void user_is_logged_in() {
        Assertions.assertTrue(loginPage.isUserLoggedIn());
    }

    @Given("Open login page")
    public void open_login_page() {
        HomePage homePage = new HomePage(driver, wait);
        loginPage = homePage.goToLoginPage();
    }

    @Given("Open homepage")
    public void open_homepage() {
        homePage = new HomePage(driver, wait);
    }

    @When("I search using {string} phrase")
    public void i_search_using_phrase(String phrase) {
        searchResultsPage = homePage.searchForPhrase(phrase);
    }

}
